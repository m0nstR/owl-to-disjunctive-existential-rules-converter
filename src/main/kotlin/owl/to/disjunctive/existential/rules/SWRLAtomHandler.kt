package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    SWRL Atom Handler

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression
import org.semanticweb.owlapi.model.SWRLAtom
import org.semanticweb.owlapi.model.SWRLClassAtom
import org.semanticweb.owlapi.model.SWRLObjectPropertyAtom
import org.semanticweb.owlapi.model.SWRLVariable
import org.semanticweb.rulewerk.core.model.api.PositiveLiteral
import org.semanticweb.rulewerk.core.model.implementation.Expressions

fun convertSwrlAtom(swrlAtom: SWRLAtom): PositiveLiteral {
    return when (swrlAtom) {
        is SWRLClassAtom -> {
            val predicate: OWLClassExpression = swrlAtom.predicate
            val argument = swrlAtom.argument

            if (argument !is SWRLVariable) {
                throw NotImplementedError("only SWRLVariable supported as arguments in SWRL rule, got: " + argument.javaClass.simpleName)
            }

            val variableName = argument.getIRI().toString()

            if (predicate !is OWLClass) {
                throw NotImplementedError("only OWLClass supported as SWRL predicate (no complex expressions) but got: " + predicate.javaClass.simpleName)
            }

            handleClass(predicate, Expressions.makeUniversalVariable(variableName))
        }
        is SWRLObjectPropertyAtom -> {
            val predicate: OWLObjectPropertyExpression = swrlAtom.predicate
            val argument1 = swrlAtom.firstArgument
            val argument2 = swrlAtom.secondArgument

            if (argument1 !is SWRLVariable || argument2 !is SWRLVariable) {
                throw NotImplementedError("only SWRLVariable supported as arguments in SWRL rule, got: " + argument1.javaClass.simpleName + argument2.javaClass.simpleName)
            }

            handleNonSpecialObjectPropertyExpression(
                predicate,
                Expressions.makeUniversalVariable(argument1.getIRI().toString()),
                Expressions.makeUniversalVariable(argument2.getIRI().toString())
            )
        }
        else -> throw NotImplementedError("only SWRLClassAtom and SWRLObjectPropertyAtom supported in SWRL Rule, got: " + swrlAtom.javaClass.simpleName)
    }
}
