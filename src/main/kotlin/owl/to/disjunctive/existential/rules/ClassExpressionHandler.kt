package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    Class Expression Handler

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom
import org.semanticweb.owlapi.model.OWLDataExactCardinality
import org.semanticweb.owlapi.model.OWLDataFactory
import org.semanticweb.owlapi.model.OWLDataHasValue
import org.semanticweb.owlapi.model.OWLDataMaxCardinality
import org.semanticweb.owlapi.model.OWLDataMinCardinality
import org.semanticweb.owlapi.model.OWLDataSomeValuesFrom
import org.semanticweb.owlapi.model.OWLObjectExactCardinality
import org.semanticweb.owlapi.model.OWLObjectHasSelf
import org.semanticweb.owlapi.model.OWLObjectHasValue
import org.semanticweb.owlapi.model.OWLObjectIntersectionOf
import org.semanticweb.owlapi.model.OWLObjectMaxCardinality
import org.semanticweb.owlapi.model.OWLObjectMinCardinality
import org.semanticweb.owlapi.model.OWLObjectOneOf
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom
import org.semanticweb.owlapi.model.OWLObjectUnionOf
import org.semanticweb.rulewerk.core.model.api.Conjunction
import org.semanticweb.rulewerk.core.model.api.Disjunction
import org.semanticweb.rulewerk.core.model.api.PositiveLiteral
import org.semanticweb.rulewerk.core.model.api.Rule
import org.semanticweb.rulewerk.core.model.api.Term
import org.semanticweb.rulewerk.core.model.implementation.Expressions
import uk.ac.manchester.cs.owl.owlapi.OWLObjectMinCardinalityImpl
import kotlin.streams.toList

fun handleClassExpression(classExpression: OWLClassExpression, term: Term, owlDataFactory: OWLDataFactory, variableSuffix: String, ignoreThingAndNothingCheck: Boolean = false): Pair<Disjunction<Conjunction<PositiveLiteral>>, Sequence<Rule>>? {
    return when (classExpression) {
        is OWLClass -> {
            Pair(Expressions.makePositiveConjunction(handleClass(classExpression, term, ignoreThingAndNothingCheck)), sequenceOf())
        }
        is OWLObjectHasValue -> {
            Pair(handleObjectPropertyExpression(classExpression.property, term, Expressions.makeAbstractConstant(classExpression.filler.toStringID()), owlDataFactory, true), sequenceOf())
        }
        is OWLObjectHasSelf -> {
            Pair(Expressions.makePositiveConjunction(handleNonSpecialObjectPropertyExpression(classExpression.getProperty(), term, term)), sequenceOf())
        }
        is OWLObjectUnionOf -> {
            val operands = classExpression.operands().toList()

            if (operands.any { it.isOWLThing() }) {
                return Pair(handleClass(owlDataFactory.getOWLThing(), term, true), sequenceOf())
            }

            operands
                .filter { !it.isOWLNothing() }
                .mapIndexed { i, op -> handleClassExpression(op, term, owlDataFactory, "${variableSuffix}_$i", ignoreThingAndNothingCheck) }
                .filterNotNull()
                .reduceOrNull { (accDisj, accAuxRules), (disj, auxRules) ->
                    Pair(
                        Expressions.makeDisjunction(accDisj.conjunctions.map { it.literals } + disj.conjunctions.map { it.literals }),
                        accAuxRules + auxRules
                    )
                } ?: Pair(handleClass(owlDataFactory.getOWLNothing(), term, true), sequenceOf())
        }
        is OWLObjectIntersectionOf -> {
            val operands = classExpression.operands().toList()

            if (operands.any { it.isOWLNothing() }) {
                return Pair(handleClass(owlDataFactory.getOWLNothing(), term, true), sequenceOf())
            }

            operands
                .filter { !it.isOWLThing() }
                .mapIndexed { i, op -> handleClassExpression(op, term, owlDataFactory, "${variableSuffix}_$i", ignoreThingAndNothingCheck) }
                .filterNotNull()
                .reduceOrNull { (accDisj, accAuxRules), (disj, auxRules) ->
                    val newDisj = conjunctDisjunctions(accDisj, disj)

                    Pair(newDisj, accAuxRules + auxRules)
                } ?: Pair(handleClass(owlDataFactory.getOWLThing(), term, true), sequenceOf())
        }
        is OWLObjectMinCardinality -> {
            handleMinCardinality(classExpression, term, owlDataFactory, "${variableSuffix}_MIN")
        }
        is OWLObjectMaxCardinality -> {
            // since we have no support for term equality, we just ignore max cardinality
            null
        }
        is OWLObjectExactCardinality -> {
            // since we do not handle max cardinality, we handle this just like min cardinality
            handleMinCardinality(OWLObjectMinCardinalityImpl(classExpression.property, classExpression.cardinality, classExpression.filler), term, owlDataFactory, "${variableSuffix}_EXACT")
        }
        is OWLObjectSomeValuesFrom -> {
            handleMinCardinality(OWLObjectMinCardinalityImpl(classExpression.property, 1, classExpression.filler), term, owlDataFactory, "${variableSuffix}_SOME")
        }
        is OWLObjectOneOf -> {
            // since we have no support for term equality, we just ignore ObjectOneOf
            null
        }
        is OWLDataAllValuesFrom, is OWLDataHasValue, is OWLDataMinCardinality, is OWLDataMaxCardinality, is OWLDataExactCardinality, is OWLDataSomeValuesFrom -> {
            // we ignore data class expressions
            null
        }
        else -> throw NotImplementedError("the following class expression is not supported, got: " + classExpression.javaClass.simpleName)
    }
}

fun handleClass(owlClass: OWLClass, term: Term, ignoreThingAndNothingCheck: Boolean = false): PositiveLiteral {
    val isSpecialClass = owlClass.isOWLThing() || owlClass.isOWLNothing()

    if (!ignoreThingAndNothingCheck && isSpecialClass) {
        throw NotImplementedError("we cannot handle Thing and Nothing class expression yet")
    }

    val iri = owlClass.getIRI()
    val literalName = if (isSpecialClass) {
        iri.toString()
    } else {
        // prevent name clashed between classes and properties
        "${iri}_CLASS"
    }

    return Expressions.makePositiveLiteral(
        literalName,
        term
    )
}
