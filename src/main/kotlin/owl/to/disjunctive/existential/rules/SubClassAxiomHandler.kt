package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    Sub Class Axiom Handler

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataFactory
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom
import org.semanticweb.owlapi.model.OWLObjectComplementOf
import org.semanticweb.rulewerk.core.model.api.Rule
import org.semanticweb.rulewerk.core.model.implementation.Expressions

fun convertSubClassAxiom(subClass: OWLClassExpression, superClass: OWLClassExpression, owlDataFactory: OWLDataFactory): Sequence<Rule>? {
    val frontier = Expressions.makeUniversalVariable("X")

    if (subClass.isOWLNothing() || superClass.isOWLThing()) {
        // Nothing in body will never fire thus the rule can be omitted
        return null
    }

    val (bodyDisjunction, bodyAuxRules) = handleClassExpression(subClass, frontier, owlDataFactory, "BODY", true) ?: return null
    val sanitizedBodyDisjunction = bodyDisjunction.getBodyVariant(owlDataFactory)

    // NOTE: we do not support AllValuesFrom in nested class expressions, thus the special handling here
    if (superClass is OWLObjectAllValuesFrom) {
        val propertyExpression = superClass.getProperty()
        val filler = superClass.getFiller()

        if (filler.isOWLThing()) {
            // tautological, can therefore be omitted
            return null
        }

        val universalVariable = Expressions.makeUniversalVariable("Y")

        val propertyLiteral = handleNonSpecialObjectPropertyExpression(propertyExpression, frontier, universalVariable)

        val (headDisjunction, headAuxRules) = handleClassExpression(filler, universalVariable, owlDataFactory, "HEAD_ALLVALUES", true) ?: return null

        if (headDisjunction.conjunctions.any { it.literals.map { it.predicate.name } == listOf(owlDataFactory.getOWLThing().getIRI().toString()) }) {
            return null
        }

        return sanitizedBodyDisjunction.map {
            Expressions.makePositiveLiteralsRule(
                headDisjunction,
                Expressions.makePositiveConjunction(propertyLiteral + it.literals.filter { it.predicate.name != owlDataFactory.getOWLThing().getIRI().toString() })
            )
        } + bodyAuxRules + headAuxRules
    } else if (superClass is OWLObjectComplementOf) {
        val innerExpr = superClass.getOperand()

        val (innerDisj, auxRules) = handleClassExpression(innerExpr, frontier, owlDataFactory, "HEAD_COMPLEMENT", true) ?: return null

        val nothing = handleClass(owlDataFactory.getOWLNothing(), frontier, true)

        return conjunctDisjunctions(bodyDisjunction, innerDisj)
            .getBodyVariant(owlDataFactory)
            .map {
                Expressions.makePositiveLiteralsRule(
                    nothing,
                    it
                )
            } + bodyAuxRules + auxRules
    }

    val (headDisjunction, headAuxRules) = handleClassExpression(superClass, frontier, owlDataFactory, "HEAD", true) ?: return null

    if (headDisjunction.conjunctions.any { it.literals.map { it.predicate.name } == listOf(owlDataFactory.getOWLThing().getIRI().toString()) }) {
        return null
    }

    return sanitizedBodyDisjunction.map {
        Expressions.makePositiveLiteralsRule(
            headDisjunction,
            it
        )
    } + bodyAuxRules + headAuxRules
}
