package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    Owl To Rules Converter

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.owlapi.model.OWLAnnotationAssertionAxiom
import org.semanticweb.owlapi.model.OWLAnnotationPropertyDomainAxiom
import org.semanticweb.owlapi.model.OWLAnnotationPropertyRangeAxiom
import org.semanticweb.owlapi.model.OWLAsymmetricObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLAxiom
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom
import org.semanticweb.owlapi.model.OWLDataFactory
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom
import org.semanticweb.owlapi.model.OWLDatatypeDefinitionAxiom
import org.semanticweb.owlapi.model.OWLDeclarationAxiom
import org.semanticweb.owlapi.model.OWLDifferentIndividualsAxiom
import org.semanticweb.owlapi.model.OWLDisjointClassesAxiom
import org.semanticweb.owlapi.model.OWLDisjointDataPropertiesAxiom
import org.semanticweb.owlapi.model.OWLDisjointObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom
import org.semanticweb.owlapi.model.OWLEquivalentDataPropertiesAxiom
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLFunctionalDataPropertyAxiom
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLHasKeyAxiom
import org.semanticweb.owlapi.model.OWLInverseFunctionalObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom
import org.semanticweb.owlapi.model.OWLIrreflexiveObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLNegativeDataPropertyAssertionAxiom
import org.semanticweb.owlapi.model.OWLNegativeObjectPropertyAssertionAxiom
import org.semanticweb.owlapi.model.OWLObject
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLReflexiveObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLSameIndividualAxiom
import org.semanticweb.owlapi.model.OWLSubAnnotationPropertyOfAxiom
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom
import org.semanticweb.owlapi.model.OWLTransitiveObjectPropertyAxiom
import org.semanticweb.owlapi.model.SWRLRule
import org.semanticweb.rulewerk.core.model.api.Fact
import org.semanticweb.rulewerk.core.model.api.Rule
import org.semanticweb.rulewerk.core.model.api.Statement
import org.semanticweb.rulewerk.core.model.implementation.Expressions
import uk.ac.manchester.cs.owl.owlapi.OWLObjectUnionOfImpl
import java.util.stream.Stream
import kotlin.streams.asSequence
import kotlin.streams.toList

fun convertOwlToRules(ontology: OWLOntology, owlDataFactory: OWLDataFactory, factsIncluded: Boolean): List<Statement> {
    val axiomStream: Stream<OWLAxiom> = ontology.axioms()
    val axioms: Sequence<OWLAxiom> = axiomStream.asSequence()

    // Cases
    // https://iccl.inf.tu-dresden.de/w/images/2/2a/Carral-Kroetzsch-ALCHIQ-rule-rewritings-IJCAI-2020-TR-2.pdf

    val rulesAndFactsForAxioms: List<Statement> = axioms
        .map {
            when (it) {
                // we can safely omit Declarations for existential rules
                is OWLDeclarationAxiom -> null

                // we ignore everything that has to do with individuals unless the factsIncluded flag is set
                is OWLClassAssertionAxiom -> {
                    if (factsIncluded) {
                        val classExpression = it.getClassExpression()

                        if (classExpression is OWLClass) {
                            if (classExpression.isOWLNothing()) {
                                throw Exception("Facts for OWLNothing should not occur...")
                            }

                            val positiveLiteral = handleClass(classExpression, Expressions.makeAbstractConstant(it.getIndividual().toStringID()), true)

                            // NOTE: Type is not inferred correctly without exclicitly converting this to Statement
                            val fact: Statement = Expressions.makeFact(positiveLiteral.predicate, positiveLiteral.arguments)
                            sequenceOf(fact)
                        } else {
                            throw NotImplementedError("complex class expression in facts not supported")
                        }
                    } else {
                        null
                    }
                }
                is OWLObjectPropertyAssertionAxiom -> {
                    if (factsIncluded) {
                        if (it.getProperty().isBottomEntity()) {
                            throw Exception("Facts for Bottom should not occur...")
                        }

                        val positiveLiterals = handleObjectPropertyExpression(it.getProperty(), Expressions.makeAbstractConstant(it.getSubject().toStringID()), Expressions.makeAbstractConstant(it.getObject().toStringID()), owlDataFactory, true)

                        positiveLiterals.literals.asSequence().map { Expressions.makeFact(it.predicate, it.arguments) }
                    } else {
                        null
                    }
                }
                is OWLNegativeObjectPropertyAssertionAxiom -> {
                    if (factsIncluded) {
                        val const1 = Expressions.makeAbstractConstant(it.getSubject().toStringID())
                        val const2 = Expressions.makeAbstractConstant(it.getObject().toStringID())
                        val positiveLiteral = handleObjectPropertyExpression(it.getProperty(), const1, const2, owlDataFactory)

                        sequenceOf(
                            Expressions.makePositiveLiteralsRule(
                                handleObjectPropertyExpression(
                                    owlDataFactory.getOWLBottomObjectProperty(),
                                    const1,
                                    const2,
                                    owlDataFactory,
                                    true // ignore top/bottom check
                                ),
                                positiveLiteral
                            )
                        )
                    } else {
                        null
                    }
                }
                // NOTE: we drop the following because of lack of support for equality
                is OWLDifferentIndividualsAxiom -> null
                is OWLSameIndividualAxiom -> null

                is OWLSubClassOfAxiom -> {
                    convertSubClassAxiom(it.getSubClass(), it.getSuperClass(), owlDataFactory)
                }
                is OWLDisjointClassesAxiom -> {
                    val classExpressions = it.operands().toList()

                    val frontier = Expressions.makeUniversalVariable("X")

                    val nothing = handleClass(owlDataFactory.getOWLNothing(), frontier, true)

                    val classResults = classExpressions.map { handleClassExpression(it, frontier, owlDataFactory, "BODY_DISJOINTCLASSES", true) }.filterNotNull()

                    val result: Sequence<Rule> = classResults.subList(0, classResults.size - 1).flatMapIndexed { i, res1 ->
                        classResults.subList(i + 1, classResults.size).flatMap { res2 ->
                            conjunctDisjunctions(res1.first, res2.first).getBodyVariant(owlDataFactory).map {
                                Expressions.makePositiveLiteralsRule(
                                    nothing,
                                    it
                                )
                            }
                        }
                    }.asSequence() + classResults.flatMap { it.second }.asSequence()

                    result
                }
                is OWLEquivalentClassesAxiom -> {
                    val classExpressions = it.operands().toList()

                    val frontier = Expressions.makeUniversalVariable("X")

                    val classResults = classExpressions.map { handleClassExpression(it, frontier, owlDataFactory, "BODY_EQUIVALENTCLASSES", true) }.filterNotNull()

                    val result: Sequence<Rule> = classResults.flatMapIndexed { i, res1 ->
                        classResults.flatMapIndexed { j, res2 ->
                            if (i == j) {
                                sequenceOf()
                            } else {
                                res1.first.getBodyVariant(owlDataFactory).map {
                                    Expressions.makePositiveLiteralsRule(
                                        res2.first,
                                        it
                                    )
                                }
                            }
                        }
                    }.asSequence() + classResults.flatMap { it.second }.asSequence()

                    result
                }
                is OWLDisjointUnionAxiom -> {
                    val disjUnion = it.operands().toList()
                    val owlClass = it.getOWLClass()

                    val frontier = Expressions.makeUniversalVariable("X")

                    val nothing = handleClass(owlDataFactory.getOWLNothing(), frontier, true)

                    val classExprResults = disjUnion.map { handleClassExpression(it, frontier, owlDataFactory, "DISJOINTUNIONCLASSES", true) }.filterNotNull()
                    val disjUnionResult = handleClassExpression(OWLObjectUnionOfImpl(disjUnion), frontier, owlDataFactory, "DISJOINTUNION", true)

                    val disjointnessRules: Sequence<Rule> = classExprResults.subList(0, classExprResults.size - 1).flatMapIndexed { i, res1 ->
                        classExprResults.subList(i + 1, classExprResults.size).flatMap { res2 ->
                            conjunctDisjunctions(res1.first, res2.first).getBodyVariant(owlDataFactory).map {
                                Expressions.makePositiveLiteralsRule(
                                    nothing,
                                    it
                                )
                            }
                        }
                    }.asSequence() + classExprResults.flatMap { it.second }.asSequence()

                    val equivalenceRules = if (disjUnionResult == null) { sequenceOf() } else {
                        sequenceOf(
                            Expressions.makePositiveLiteralsRule(
                                disjUnionResult.first,
                                handleClass(owlClass, frontier)
                            )
                        ) + disjUnionResult.first.getBodyVariant(owlDataFactory).map {
                            Expressions.makePositiveLiteralsRule(
                                handleClass(owlClass, frontier),
                                it
                            )
                        } + disjUnionResult.second
                    }

                    disjointnessRules + equivalenceRules
                }
                is OWLDisjointObjectPropertiesAxiom -> {
                    val frontier1 = Expressions.makeUniversalVariable("X")
                    val frontier2 = Expressions.makeUniversalVariable("Y")

                    val operands = it.operands().toList()

                    operands.withIndex().flatMap { (outerIndex, operand1) ->
                        operands.mapIndexed { innerIndex, operand2 ->
                            if (innerIndex <= outerIndex) {
                                null
                            } else {
                                Expressions.makePositiveLiteralsRule(
                                    handleObjectPropertyExpression(
                                        owlDataFactory.getOWLBottomObjectProperty(),
                                        frontier1,
                                        frontier2,
                                        owlDataFactory,
                                        true // ignore top/bottom check
                                    ),
                                    Expressions.makePositiveConjunction(
                                        handleNonSpecialObjectPropertyExpression(operand1, frontier1, frontier2),
                                        handleNonSpecialObjectPropertyExpression(operand2, frontier1, frontier2)
                                    )
                                )
                            }
                        }.filterNotNull()
                    }.asSequence()
                }
                is OWLEquivalentObjectPropertiesAxiom -> {
                    val frontier1 = Expressions.makeUniversalVariable("X")
                    val frontier2 = Expressions.makeUniversalVariable("Y")

                    val operands = it.operands().toList()

                    operands.flatMapIndexed { outerIndex, operand1 ->
                        operands.flatMapIndexed { innerIndex, operand2 ->
                            if (innerIndex <= outerIndex) {
                                listOf()
                            } else {
                                listOf(
                                    Expressions.makePositiveLiteralsRule(
                                        handleObjectPropertyExpression(operand2, frontier1, frontier2, owlDataFactory),
                                        handleObjectPropertyExpression(operand1, frontier1, frontier2, owlDataFactory)
                                    ),
                                    Expressions.makePositiveLiteralsRule(
                                        handleObjectPropertyExpression(operand1, frontier1, frontier2, owlDataFactory),
                                        handleObjectPropertyExpression(operand2, frontier1, frontier2, owlDataFactory)
                                    )
                                )
                            }
                        }
                    }.asSequence()
                }
                is OWLSubObjectPropertyOfAxiom -> {
                    val subProperty = it.getSubProperty()
                    val superProperty = it.getSuperProperty()

                    val frontier1 = Expressions.makeUniversalVariable("X")
                    val frontier2 = Expressions.makeUniversalVariable("Y")

                    if (superProperty.isTopEntity() || subProperty.isBottomEntity()) {
                        // bottom as sub property will never fire and
                        // top in head is tautological, so we can ignore those cases
                        null
                    } else {
                        sequenceOf(
                            Expressions.makePositiveLiteralsRule(
                                // we allow Bottom Property in head (Top is handled above)
                                handleObjectPropertyExpression(superProperty, frontier1, frontier2, owlDataFactory, true),
                                // we allow Top in body (Bottom is handled above)
                                handleObjectPropertyExpression(subProperty, frontier1, frontier2, owlDataFactory, true)
                            )
                        )
                    }
                }
                is OWLSubPropertyChainOfAxiom -> {
                    val propertyChain = it.getPropertyChain()
                    val superProperty = it.getSuperProperty()

                    val frontierStart = Expressions.makeUniversalVariable("X0")
                    val frontierEnd = Expressions.makeUniversalVariable("X${propertyChain.size}")

                    val bodyLiterals = propertyChain.mapIndexed { i, p ->
                        val frontier1 = Expressions.makeUniversalVariable("X$i")
                        val frontier2 = Expressions.makeUniversalVariable("X${i + 1}")
                        handleNonSpecialObjectPropertyExpression(p, frontier1, frontier2)
                    }

                    sequenceOf(
                        Expressions.makePositiveLiteralsRule(
                            handleObjectPropertyExpression(superProperty, frontierStart, frontierEnd, owlDataFactory),
                            Expressions.makePositiveConjunction(bodyLiterals)
                        )
                    )
                }
                is OWLObjectPropertyDomainAxiom -> {
                    val property = it.getProperty()
                    val classExpr = it.getDomain()

                    val frontier = Expressions.makeUniversalVariable("X")
                    val extraBodyVar = Expressions.makeUniversalVariable("Y")

                    val bodyLiteral = handleNonSpecialObjectPropertyExpression(property, frontier, extraBodyVar)

                    val classRes = handleClassExpression(classExpr, frontier, owlDataFactory, "HEAD_PROPERTYDOMAIN", true)

                    if (classRes == null) {
                        null
                    } else {
                        val (headDisj, auxRules) = classRes

                        sequenceOf(
                            Expressions.makePositiveLiteralsRule(
                                headDisj,
                                Expressions.makePositiveConjunction(bodyLiteral)
                            )
                        ) + auxRules
                    }
                }
                is OWLObjectPropertyRangeAxiom -> {
                    val property = it.getProperty()
                    val classExpr = it.getRange()

                    val frontier = Expressions.makeUniversalVariable("X")
                    val extraBodyVar = Expressions.makeUniversalVariable("Y")

                    val bodyLiteral = handleNonSpecialObjectPropertyExpression(property, extraBodyVar, frontier)

                    val classRes = handleClassExpression(classExpr, frontier, owlDataFactory, "HEAD_PROPERTYRANGE", true)

                    if (classRes == null) {
                        null
                    } else {
                        val (headDisj, auxRules) = classRes

                        sequenceOf(
                            Expressions.makePositiveLiteralsRule(
                                headDisj,
                                Expressions.makePositiveConjunction(bodyLiteral)
                            )
                        ) + auxRules
                    }
                }
                is OWLIrreflexiveObjectPropertyAxiom -> {
                    val property = it.getProperty()

                    val frontier = Expressions.makeUniversalVariable("X")

                    sequenceOf(
                        Expressions.makePositiveLiteralsRule(
                            handleObjectPropertyExpression(
                                owlDataFactory.getOWLBottomObjectProperty(),
                                frontier,
                                frontier,
                                owlDataFactory,
                                true // ignore top/bottom check
                            ),
                            handleObjectPropertyExpression(property, frontier, frontier, owlDataFactory, true)
                        )
                    )
                }
                is OWLReflexiveObjectPropertyAxiom -> {
                    val property = it.getProperty()

                    val frontier = Expressions.makeUniversalVariable("X")

                    val thing = handleClass(owlDataFactory.getOWLThing(), frontier, true)

                    sequenceOf(
                        Expressions.makePositiveLiteralsRule(
                            handleObjectPropertyExpression(property, frontier, frontier, owlDataFactory, true),
                            thing
                        )
                    )
                }
                is OWLAsymmetricObjectPropertyAxiom -> {
                    val property = it.getProperty()

                    val frontier1 = Expressions.makeUniversalVariable("X")
                    val frontier2 = Expressions.makeUniversalVariable("Y")

                    sequenceOf(
                        Expressions.makePositiveLiteralsRule(
                            handleObjectPropertyExpression(
                                owlDataFactory.getOWLBottomObjectProperty(),
                                frontier1,
                                frontier2,
                                owlDataFactory,
                                true // ignore top/bottom check
                            ),
                            Expressions.makePositiveConjunction(
                                handleNonSpecialObjectPropertyExpression(property, frontier2, frontier1),
                                handleNonSpecialObjectPropertyExpression(property, frontier1, frontier2)
                            )
                        )
                    )
                }
                is OWLSymmetricObjectPropertyAxiom -> {
                    val property = it.getProperty()

                    val frontier1 = Expressions.makeUniversalVariable("X")
                    val frontier2 = Expressions.makeUniversalVariable("Y")

                    sequenceOf(
                        Expressions.makePositiveLiteralsRule(
                            handleObjectPropertyExpression(property, frontier2, frontier1, owlDataFactory, true),
                            handleObjectPropertyExpression(property, frontier1, frontier2, owlDataFactory, true)
                        )
                    )
                }
                is OWLTransitiveObjectPropertyAxiom -> {
                    val property = it.getProperty()

                    val frontier1 = Expressions.makeUniversalVariable("X")
                    val frontier2 = Expressions.makeUniversalVariable("Z")
                    val fillerVar = Expressions.makeUniversalVariable("Y")

                    sequenceOf(
                        Expressions.makePositiveLiteralsRule(
                            handleNonSpecialObjectPropertyExpression(property, frontier1, frontier2),
                            Expressions.makePositiveConjunction(handleNonSpecialObjectPropertyExpression(property, frontier1, fillerVar), handleNonSpecialObjectPropertyExpression(property, fillerVar, frontier2))
                        )
                    )
                }
                is OWLInverseObjectPropertiesAxiom -> {
                    val firstProperty = it.getFirstProperty()
                    val secondProperty = it.getSecondProperty()

                    val frontier1 = Expressions.makeUniversalVariable("X")
                    val frontier2 = Expressions.makeUniversalVariable("Y")

                    sequenceOf(
                        Expressions.makePositiveLiteralsRule(
                            handleObjectPropertyExpression(secondProperty, frontier2, frontier1, owlDataFactory, true),
                            handleObjectPropertyExpression(firstProperty, frontier1, frontier2, owlDataFactory, true)
                        ),
                        Expressions.makePositiveLiteralsRule(
                            handleObjectPropertyExpression(firstProperty, frontier2, frontier1, owlDataFactory, true),
                            handleObjectPropertyExpression(secondProperty, frontier1, frontier2, owlDataFactory, true)
                        )
                    )
                }
                is SWRLRule -> {
                    val swrlHead = it.head().toList()
                    val swrlBody = it.body().toList()

                    sequenceOf(
                        Expressions.makePositiveLiteralsRule(
                            Expressions.makePositiveConjunction(swrlHead.map { convertSwrlAtom(it) }),
                            Expressions.makePositiveConjunction(swrlBody.map { convertSwrlAtom(it) })
                        )
                    )
                }
                is OWLFunctionalObjectPropertyAxiom, is OWLInverseFunctionalObjectPropertyAxiom, is OWLHasKeyAxiom -> {
                    // since we have no support for term equality, we just ignore max cardinality (functionality is essentially the same, haskey is also similar)
                    null
                }
                // we ignore data axioms
                is OWLAnnotationAssertionAxiom, is OWLAnnotationPropertyDomainAxiom, is OWLAnnotationPropertyRangeAxiom, is OWLDataPropertyAssertionAxiom, is OWLDataPropertyDomainAxiom, is OWLDataPropertyRangeAxiom, is OWLDatatypeDefinitionAxiom, is OWLSubDataPropertyOfAxiom, is OWLDisjointDataPropertiesAxiom, is OWLEquivalentDataPropertiesAxiom, is OWLFunctionalDataPropertyAxiom, is OWLNegativeDataPropertyAssertionAxiom, is OWLSubAnnotationPropertyOfAxiom -> {
                    null
                }
                else -> throw NotImplementedError("axiom type not supported, got: " + it.javaClass.simpleName)
            }
        }
        .filterNotNull()
        .flatten()
        .toList()

    // for each predicate P we introduce a rule
    // P(x) -> OWLThing(x)
    val prediactes = rulesAndFactsForAxioms
        .flatMap {
            if (it is Rule) {
                it.head.conjunctions.flatMap { it.literals.map { it.predicate } } +
                    it.body.conjunctions.flatMap { it.literals.map { it.predicate } }
            } else if (it is Fact) {
                listOf(it.predicate)
            } else {
                throw Exception("there should only be Rules and Facts here")
            }
        }
        .toSet()

    val rulesForThing: Sequence<Statement> = prediactes
        .asSequence()
        .map { predicate ->
            val correspondingOwlObject: OWLObject = if (predicate.arity == 1) {
                owlDataFactory.getOWLClass(predicate.name)
            } else if (predicate.arity == 2) {
                owlDataFactory.getOWLObjectProperty(predicate.name)
            } else {
                throw Exception("perdicate with arity other than 1 or 2 occured; this should never happen")
            }

            // do not introduce rule to thing if we have a special owl predicate
            if (correspondingOwlObject.isTopEntity() || correspondingOwlObject.isBottomEntity()) {
                return@map null
            }

            val frontier = (1..predicate.arity).map { Expressions.makeUniversalVariable("X$it") }

            Expressions.makePositiveLiteralsRule(
                Expressions.makePositiveConjunction(frontier.map { handleClass(owlDataFactory.getOWLThing(), it, true) }),
                Expressions.makePositiveLiteral(predicate, frontier)
            )
        }
        .filterNotNull()

    // we introduce a single rule
    // OWLThing(x) \land OWLThing(y) -> TopObjectProperty(x, y)
    // val frontier1 = Expressions.makeUniversalVariable("X")
    // val frontier2 = Expressions.makeUniversalVariable("Y")
    // val ruleForTopObjectProperty = Expressions.makePositiveLiteralsRule(
    //     handleObjectPropertyExpression(
    //         owlDataFactory.getOWLTopObjectProperty(),
    //         frontier1,
    //         frontier2,
    //         true // ignore top/bottom check
    //     ),
    //     Expressions.makePositiveConjunction(
    //         handleClass(owlDataFactory.getOWLThing(), frontier1, true),
    //         handleClass(owlDataFactory.getOWLThing(), frontier2, true)
    //     )
    // )

    return (rulesAndFactsForAxioms + rulesForThing/* + ruleForTopObjectProperty*/).toList()
}
