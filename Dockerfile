FROM openjdk:8-jre

WORKDIR /app

# tar unpacking is intentional
ADD build/distributions/owl-to-disjunctive-existential-rules.tar /app/

ENTRYPOINT ["./owl-to-disjunctive-existential-rules/bin/owl-to-disjunctive-existential-rules"]
