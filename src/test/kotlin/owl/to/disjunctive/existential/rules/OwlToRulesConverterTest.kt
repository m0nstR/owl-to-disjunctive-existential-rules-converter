package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    Owl To Rules Converter Test

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory
import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase
import java.io.File
import java.io.StringWriter
import kotlin.test.assertEquals

class OwlToRulesConverterTest {
    @TestFactory
    fun testAllInTestDataDirectory() =
        File("./test-data/").walkTopDown()
            .filter { """.+\.xml$""".toRegex().matches(it.name) }
            .map { file ->
                dynamicTest("correctly translate ${file.path}") {
                    val target = File("${file.path}.rules").readText().lines().toSet()

                    val ontologyManager = OWLManager.createOWLOntologyManager()
                    val ontology = ontologyManager
                        .loadOntologyFromOntologyDocument(file.inputStream())

                    val owlDataFactory = ontologyManager.getOWLDataFactory()

                    val rulesAndFacts = convertOwlToRules(ontology, owlDataFactory, true)

                    val kb = KnowledgeBase()
                    kb.addStatements(rulesAndFacts)

                    val writer = StringWriter()

                    writer.also {
                        kb.writeKnowledgeBase(it)
                        it.flush()
                    }

                    val computed = writer.toString().lines().toSet()

                    assertEquals(target, computed)
                }
            }
            .toList()
}
