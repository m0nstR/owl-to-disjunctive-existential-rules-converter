package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    Object Property Handler

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.owlapi.model.OWLDataFactory
import org.semanticweb.owlapi.model.OWLObjectInverseOf
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression
import org.semanticweb.rulewerk.core.model.api.Conjunction
import org.semanticweb.rulewerk.core.model.api.PositiveLiteral
import org.semanticweb.rulewerk.core.model.api.Term
import org.semanticweb.rulewerk.core.model.implementation.Expressions

fun handleNonSpecialObjectPropertyExpression(propertyExpression: OWLObjectPropertyExpression, term1: Term, term2: Term): PositiveLiteral {
    val isSpecialProperty = propertyExpression.isTopEntity() || propertyExpression.isBottomEntity()

    if (isSpecialProperty) {
        throw NotImplementedError("we cannot handle top object property and bottom object property here")
    }

    val isInverseProperty = propertyExpression is OWLObjectInverseOf

    val property = propertyExpression.getNamedProperty()

    val iri = property.getIRI()

    return Expressions.makePositiveLiteral(
        // prevent name clashed between classes and properties
        "${iri}_PROPERTY",
        if (isInverseProperty) { listOf(term2, term1) } else { listOf(term1, term2) }
    )
}

fun handleObjectPropertyExpression(propertyExpression: OWLObjectPropertyExpression, term1: Term, term2: Term, owlDataFactory: OWLDataFactory, ignoreTopAndBottomCheck: Boolean = false): Conjunction<PositiveLiteral> =
    if (ignoreTopAndBottomCheck && propertyExpression.isTopEntity()) {
        Expressions.makePositiveConjunction(
            Expressions.makePositiveLiteral(
                owlDataFactory.getOWLThing().getIRI().toString(),
                term1
            ),
            Expressions.makePositiveLiteral(
                owlDataFactory.getOWLThing().getIRI().toString(),
                term2
            )
        )
    } else if (ignoreTopAndBottomCheck && propertyExpression.isBottomEntity()) {
        Expressions.makePositiveConjunction(
            Expressions.makePositiveLiteral(
                owlDataFactory.getOWLNothing().getIRI().toString(),
                term1
            ),
            Expressions.makePositiveLiteral(
                owlDataFactory.getOWLNothing().getIRI().toString(),
                term2
            )
        )
    } else {
        Expressions.makePositiveConjunction(handleNonSpecialObjectPropertyExpression(propertyExpression, term1, term2))
    }
