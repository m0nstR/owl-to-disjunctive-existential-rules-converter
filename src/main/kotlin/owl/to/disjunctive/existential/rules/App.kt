package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    App

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.rulewerk.core.reasoner.KnowledgeBase

fun main(args: Array<String>) {
    val parser = ArgParser("owl-to-existential-rules-converter")
    val factsIncluded by parser.option(ArgType.Boolean, shortName = "f", description = "Include facts when translating (i.e. OWLClassAssertionAxioms and OWLObjectPropertyAssertionAxiomx) instead of only rules.").default(false)
    parser.parse(args)

    val ontologyManager = OWLManager.createOWLOntologyManager()

    val ontology = ontologyManager
        .loadOntologyFromOntologyDocument(System.`in`)
    val owlDataFactory = ontologyManager.getOWLDataFactory()

    val rules = convertOwlToRules(ontology, owlDataFactory, factsIncluded)

    val kb = KnowledgeBase()
    kb.addStatements(rules)

    System.out.writer().also {
        kb.writeKnowledgeBase(it)
        it.flush()
    }
}
