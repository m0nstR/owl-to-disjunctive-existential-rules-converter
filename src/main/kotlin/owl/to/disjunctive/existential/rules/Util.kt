package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    Util

    Copyright 2023 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.owlapi.model.OWLDataFactory
import org.semanticweb.rulewerk.core.model.api.Conjunction
import org.semanticweb.rulewerk.core.model.api.Disjunction
import org.semanticweb.rulewerk.core.model.api.ExistentialVariable
import org.semanticweb.rulewerk.core.model.api.Literal
import org.semanticweb.rulewerk.core.model.api.PositiveLiteral
import org.semanticweb.rulewerk.core.model.implementation.Expressions

// treat list of disjunctions as a conjunction of dicjunctions and turn it into a disjunction of conjunctions
fun <T : Literal<T>>conjunctDisjunctions(a: Disjunction<Conjunction<T>>, b: Disjunction<Conjunction<T>>): Disjunction<Conjunction<T>> = Expressions.makeDisjunction(a.conjunctions.flatMap { conjA -> b.conjunctions.map { conjB -> conjA + conjB } })

// treat list of disjunctions as a conjunction of dicjunctions and turn it into a disjunction of conjunctions
fun <T : Literal<T>>conjunctDisjunctions(disjunctions: List<Disjunction<Conjunction<T>>>): Disjunction<Conjunction<T>> = disjunctions.reduce { acc, disj -> conjunctDisjunctions(acc, disj) }

fun Disjunction<Conjunction<PositiveLiteral>>.getBodyVariant(owlDataFactory: OWLDataFactory): Sequence<Conjunction<PositiveLiteral>> =
    conjunctions
        .asSequence()
        .filter { it.literals.map { it.predicate.name } != listOf(owlDataFactory.getOWLNothing().getIRI().toString()) }
        .map {
            Expressions.makePositiveConjunction(
                it.literals.map { lit ->
                    Expressions.makePositiveLiteral(
                        lit.predicate,
                        lit.arguments.map { arg ->
                            if (arg is ExistentialVariable) {
                                Expressions.makeUniversalVariable("REPLACED_EXISTENTIAL_${arg.name}")
                            } else { arg }
                        }
                    )
                }
            )
        }
