# OWL to Disjunctive Existential Rules Converter

We want to translate OWL ontologies into Disjunctive Existential Rules.
Initially this project was setup to only evaluate [Disjunctive Model Faithful Acyclicity (DMFA)](https://iccl.inf.tu-dresden.de/web/Inproceedings3348) (which is based on a [student project](https://iccl.inf.tu-dresden.de/web/Thema3509)),
but the translation is of course not limited to this notion.  

Our tool is based on the [Rulewerk](https://github.com/knowsys/rulewerk/) Library which in turn uses [OWLAPI](https://github.com/owlcs/owlapi).
Since we want to be able to create rules with disjunctions, we use our own [fork of Rulewerk](https://github.com/monsterkrampe/rulewerk).
For simplicity, this fork of rulewerk has been published to the [package registry](https://gitlab.com/m0nstR/owl-to-disjunctive-existential-rules-converter/-/packages) of this project.

Additionally we use [`kotlinx-cli`](https://github.com/Kotlin/kotlinx-cli) to build a very basic CLI for our project (see [Usage](#usage)).

In earlier versions of the tool, normalized ontologies were created using an existing [Ontology Normalizer](https://github.com/dcarralma/OntologyNormalizer) (this can of course still be done but is not necessary anymore).
For simplicity, we built a docker image `registry.gitlab.com/m0nstr/owl-to-disjunctive-existential-rules-converter/normalizer` for this normalizer which can be found in the [container registry](https://gitlab.com/m0nstR/owl-to-disjunctive-existential-rules-converter/container_registry) of this project.

## Usage

Our tool reads the ontology from `STDIN` using [`loadOntologyFromOntologyDocument`](https://owlcs.github.io/owlapi/apidocs_5/org/semanticweb/owlapi/model/OWLOntologyManager.html#loadOntologyFromOntologyDocument-java.io.InputStream-) and outputs the translated rule set to `STDOUT` (in the default serialization format of Rulewerk).

A bundled version of the application can be downloaded from the CI/CD Jobs. You obtain a `.zip` or `.tar` which contain an executable shell script and a `.bat` file as well.

Assuming that the shell script is located at `./app/bin/app`, you can translate a normalized OWL ontology as follows (to include facts and not just rules you can add `-f`; in general you can use `./app/bin/app --help` to obtain more information):

```bash
cat <path-to-owl-ontology> | ./app/bin/app # outputs to STDOUT (and STDERR)
cat <path-to-owl-ontology> | JAVA_OPTS="-Xmx8g -XX:+UseConcMarkSweepGC" ./app/bin/app # custom options for JVM
cat <path-to-owl-ontology> | ./app/bin/app >translation-result 2>translation-error-log # writes result and error to files
```

For convenience, we also offer a docker image `registry.gitlab.com/m0nstr/owl-to-disjunctive-existential-rules-converter` in our [container registry](https://gitlab.com/m0nstR/owl-to-disjunctive-existential-rules-converter/container_registry).

```bash
cat <path-to-owl-ontology> | docker run --rm -i registry.gitlab.com/m0nstr/owl-to-disjunctive-existential-rules-converter # outputs to STDOUT (and STDERR)
cat <path-to-owl-ontology> | docker run --rm -e JAVA_OPTS="-Xmx8g -XX:+UseConcMarkSweepGC" -i registry.gitlab.com/m0nstr/owl-to-disjunctive-existential-rules-converter # custom options for JVM
cat <path-to-owl-ontology> | docker run --rm -i registry.gitlab.com/m0nstr/owl-to-disjunctive-existential-rules-converter >translation-result 2>translation-error-log # writes result and error to files
```

## More Information on the Translation

**In the current Version we support arbitrary OWL ontologies as input (but throw exceptions in some cases that we do not support). There is no need anymore to normalize the ontology up front!**
Of course normalized ontologies should still work and we expect the output to be largely equivalent to ealier versions.

In earlier versions of the tool, we assumed input from an Ontology Normalizer
that produces a normal form according to the following table.  
The table also shows how the individual axioms are translated by our tool.

| Normalized Axiom | Existential rule |
| --- | --- |
| $`A_1 \sqcap \dots \sqcap A_n \sqsubseteq B`$ | $`A_1(x) \land \dots \land A_n(x) \to B(x)`$ |
| $`A \sqsubseteq B_1 \sqcup \dots \sqcup B_{n'}`$ | $`A(x) \to B_1(x) \lor \dots \lor B_{n'}(x)`$ |
| $`A \sqsubseteq \forall R.B`$ | $`A(x) \land R(x, y) \to B(y)`$ |
| $`A \sqsubseteq \mathord{\geq} m R.B`$ | $`\text{(e.g. for $m=3$)}`$<br/>$`A(x) \to \exists z_1. (R_1(x, z_1) \land B(z_1))`$<br/>$`A(x) \to \exists z_2. (R_2(x, z_2) \land B(z_2))`$<br/>$`A(x) \to \exists z_3. (R_3(x, z_3) \land B(z_3))`$<br/>$`R_1(x, y) \to R(x, y)`$<br/>$`R_2(x, y) \to R(x, y)`$<br/>$`R_3(x, y) \to R(x, y)`$<br/>$`R_1(x, y) \land R_2(x, y) \to \bot`$<br/>$`R_2(x, y) \land R_3(x, y) \to \bot`$<br/>$`R_1(x, y) \land R_3(x, y) \to \bot`$<br/>$`R(x, y) \to R_1(x, y) \lor R_2(x, y) \lor R_3(x, y)`$ <br/>Here, $`R_1,R_2,R_3`$ are fresh predicates that form a partitioning of $`R`$.|
| $`A \sqsubseteq \mathord{\leq} m' R.B`$ | $`\text{dropped (no support for equality)}`$ |
| $`A \sqsubseteq \{ a_1, \dots, a_l \}`$ | $`\text{dropped (no support for equality)}`$ |
| $`R_1 \circ \dots \circ R_k \sqsubseteq S`$ | $`R_1(x_1, x_2) \land \dots \land R_k(x_k, x_{k+1}) \to S(x_1, x_{k+1})`$ |
| $`R \sqcap S \sqsubseteq \bot`$ | $`R(x, y) \land S(x, y) \to \bot`$ |
| $`A \sqsubseteq \exists R.\text{Self}`$ | $`A(x) \to R(x, x)`$ |
| $`\exists R.\text{Self} \sqsubseteq A`$ | $`R(x, x) \to A(x)`$ |

In the table (if not stated otherwise) $`A, A_1, \dots, A_n, B, B_1, \dots, B_{n'}`$ are classes, $`R, R_1, \dots, R_k, S`$ are (inverse) properties,
$`a_1, \dots, a_l`$ are individuals, and $`m, m'`$ are integers $`\geq 0`$.

We add some more rules and conditions to mimic some important of the semantics of `OWLThing`, `OWLNothing`, `TopObjectProperty`, and `BottomObjectProperty` respectively.

*In the current version of the tool, we support way more than just the normalized axioms but we do not list them in the table above.*

