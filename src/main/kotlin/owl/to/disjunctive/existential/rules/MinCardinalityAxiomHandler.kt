package owl.to.disjunctive.existential.rules

/*
    OWL to Disjunctive Existential Rules
    Min Cardinality Axiom Handler

    Copyright 2021 Lukas Gerlach

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLClassExpression
import org.semanticweb.owlapi.model.OWLDataFactory
import org.semanticweb.owlapi.model.OWLObjectMinCardinality
import org.semanticweb.rulewerk.core.model.api.Conjunction
import org.semanticweb.rulewerk.core.model.api.Disjunction
import org.semanticweb.rulewerk.core.model.api.ExistentialVariable
import org.semanticweb.rulewerk.core.model.api.PositiveLiteral
import org.semanticweb.rulewerk.core.model.api.Rule
import org.semanticweb.rulewerk.core.model.api.Term
import org.semanticweb.rulewerk.core.model.implementation.Expressions

private fun handleExistentialPart(propertyLiteral: PositiveLiteral, filler: OWLClassExpression, existentialVariable: ExistentialVariable, owlDataFactory: OWLDataFactory, variableSuffix: String): Pair<Disjunction<Conjunction<PositiveLiteral>>, Sequence<Rule>> {
    if (filler.isOWLThing) {
        // Thing can be omitted since it is true anyway
        return Pair(Expressions.makePositiveConjunction(propertyLiteral), sequenceOf())
    }

    val (disj, auxRules) = handleClassExpression(filler, existentialVariable, owlDataFactory, variableSuffix) ?: return Pair(Expressions.makePositiveConjunction(propertyLiteral), sequenceOf())

    val newDisj = Expressions.makeDisjunction(disj.conjunctions.map { propertyLiteral + it })

    return Pair(newDisj, auxRules)
}

fun handleMinCardinality(minCardAxiom: OWLObjectMinCardinality, term: Term, owlDataFactory: OWLDataFactory, variableSuffix: String): Pair<Disjunction<Conjunction<PositiveLiteral>>, Sequence<Rule>>? {
    val cardinality = minCardAxiom.getCardinality()

    if (cardinality < 1) {
        // MinCardinality with cardinality of 0 does not carry any value...
        return null
    }

    // we could always treat minCardinality with cardinality >= 1
    // as existential quantification, losing some information in the process
    // but this should suffice for the acyclicity notion evaluation
    //
    // hoever we now do the following
    // e.g. for cardinality 2:
    // A \sqsubseteq \geq 2 R.B
    // becomes
    // A \sqsubseteq \exists R1.B
    // A \sqsubseteq \exists R2.B
    // R1 \sqsubseteq R
    // R2 \sqsubseteq R
    //
    // R \sqsubseteq R1 \lor R2
    //
    // R1 \sqcap R2 \sqsubseteq \bot

    val propertyExpression = minCardAxiom.getProperty()
    val filler = minCardAxiom.getFiller()

    val existentialVariable = Expressions.makeExistentialVariable("Z_$variableSuffix")

    if (filler is OWLClass && filler.isOWLNothing) {
        // we keep Nothing but we can ignore the object property in this case
        return Pair(handleClass(filler, existentialVariable, true), sequenceOf())
    }

    // for cardinality 1 we can skip all the auxiliary predicates
    if (cardinality == 1) {
        val propertyLiteral = handleNonSpecialObjectPropertyExpression(propertyExpression, term, existentialVariable)

        return handleExistentialPart(propertyLiteral, filler, existentialVariable, owlDataFactory, variableSuffix)
    }

    val auxiliaryVariable1 = Expressions.makeUniversalVariable("Y1")
    val auxiliaryVariable2 = Expressions.makeUniversalVariable("Y2")

    val propertyLiteral = handleNonSpecialObjectPropertyExpression(propertyExpression, auxiliaryVariable1, auxiliaryVariable2)

    val bottomProperty = handleObjectPropertyExpression(
        owlDataFactory.getOWLBottomObjectProperty(),
        auxiliaryVariable1,
        auxiliaryVariable2,
        owlDataFactory,
        true // ignore top/bottom check
    )

    val auxiliaryPropertyNames = (1..cardinality).map {
        "${propertyExpression.getNamedProperty().getIRI()}_SUBPROPERTY_$it"
    }

    val (newDisj, newAuxRules) = auxiliaryPropertyNames.mapIndexed { i, propName ->
        val existentialVariableForIthPredicate = Expressions.makeExistentialVariable("Z_aux${i}_$variableSuffix")
        // 1.
        // A \sqsubseteq \exists R1.B
        // A \sqsubseteq \exists R2.B
        val auxiliaryProperty = Expressions.makePositiveLiteral(
            propName,
            term,
            existentialVariableForIthPredicate
        )

        handleExistentialPart(auxiliaryProperty, filler, existentialVariableForIthPredicate, owlDataFactory, variableSuffix)
    }.reduce { (accDisj, accAuxRules), (disj, auxRules) ->
        val newDisj = Expressions.makeDisjunction(accDisj.conjunctions.flatMap { a -> disj.conjunctions.map { b -> a + b } })

        Pair(newDisj, accAuxRules + auxRules)
    }

    // 3 parts
    val auxRules = newAuxRules + auxiliaryPropertyNames.map {
        // 2.
        // R1 \sqsubseteq R
        // R2 \sqsubseteq R
        val auxiliaryProperty = Expressions.makePositiveLiteral(
            it,
            auxiliaryVariable1,
            auxiliaryVariable2
        )

        Expressions.makePositiveLiteralsRule(
            propertyLiteral,
            auxiliaryProperty
        )
    } + sequenceOf(
        // 3.
        // R \sqsubseteq R1 \lor R2
        Expressions.makePositiveLiteralsRule(
            Expressions.makeDisjunction(
                auxiliaryPropertyNames.map {
                    listOf(
                        Expressions.makePositiveLiteral(
                            it,
                            auxiliaryVariable1,
                            auxiliaryVariable2
                        )
                    )
                }
            ),
            propertyLiteral
        )
    ) + auxiliaryPropertyNames.withIndex().flatMap { (outerIndex, propertyName1) ->
        // 4.
        // R1 \sqcap R2 \sqsubseteq \bot
        auxiliaryPropertyNames.mapIndexed { innerIndex, propertyName2 ->
            if (innerIndex <= outerIndex) {
                null
            } else {
                Expressions.makePositiveLiteralsRule(
                    bottomProperty,
                    Expressions.makePositiveConjunction(
                        Expressions.makePositiveLiteral(
                            propertyName1,
                            auxiliaryVariable1,
                            auxiliaryVariable2
                        ),
                        Expressions.makePositiveLiteral(
                            propertyName2,
                            auxiliaryVariable1,
                            auxiliaryVariable2
                        )
                    )
                )
            }
        }.filterNotNull()
    }

    return Pair(newDisj, auxRules.asSequence())
}
